package com.avenuecode.library.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document
@NoArgsConstructor
public class Book {
	
	@Id
	private String id;
	
	private String title;
	
	private String author;
	
}
