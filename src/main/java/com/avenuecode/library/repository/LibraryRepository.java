package com.avenuecode.library.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.avenuecode.library.entity.Book;

@Repository
public interface LibraryRepository extends MongoRepository<Book, String> {
	
}
