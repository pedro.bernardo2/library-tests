package com.avenuecode.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.library.entity.Book;
import com.avenuecode.library.repository.LibraryRepository;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(propagation = Propagation.SUPPORTS)
@RequiredArgsConstructor
public class LibraryService {

	@Autowired
	private LibraryRepository libraryRepository;

	public List<Book> getAll() {
		return libraryRepository.findAll();
	}

	public Book save(Book book) {
		return libraryRepository.save(book);
	}

	public Book updateBook(String id, Book book) {
		Book updateBook = libraryRepository.findById(id).orElseThrow(
				() -> new RuntimeException(String.format("Não foi possivel encontrar o livro")));
		
		updateBook.setAuthor(book.getAuthor());
		updateBook.setTitle(book.getTitle());
		
		return libraryRepository.save(book);
		
	}
	
	public ResponseEntity<?> deleteBook(String id) {
		Book deleteBook = libraryRepository.findById(id).orElseThrow(
				() -> new RuntimeException(String.format("Não foi possivel encontrar o livro")));
		
		libraryRepository.delete(deleteBook);
		return ResponseEntity.ok().build();
		
	}
	
	public Book getBookById(String id) {
		return libraryRepository.findById(id).orElseThrow(
				() -> new RuntimeException(String.format("Não foi possivel encontrar o livro")));
	}

}
