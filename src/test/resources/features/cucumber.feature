Feature: Library Feature

	Scenario: A user gets the books
	Given the following books
	| id | author 				| title 																	 |
	| 1b | J. K. Rowling	| Harry Potter and the Philosophers Stone  |
	| 1c | Dan Brown			| Da Vinci Code														 |
		When the user requests all the books
		Then all the books are returned
		
	Scenario: A user want to update a Book
		Given the user want to update the Book title with id "1b"
		When the system gets the new title "Harry Potter and the Chamber of Secrets"
		Then the user gets the updated Book
		
	Scenario: A user want to save a Book
		When the user want to save the Book
	| id | author 				| title 																	 |
	| 1d | John Boyne			| The Boy in the Striped Pajamas  				 |
 		Then the user gets the Book saved