package com.avenuecode.library.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.avenuecode.library.entity.Book;
import com.avenuecode.library.repository.LibraryRepository;

@ExtendWith(MockitoExtension.class)
public class LibraryServiceTest {
	
	@Mock
	private LibraryRepository libraryRepository;
		
	@InjectMocks
	private LibraryService libraryService;
	
	List<Book> books = new ArrayList<>();
	
	@BeforeEach
	public void setup() {
		
		Book firstBook = new Book();
		firstBook.setId("a1");
		firstBook.setAuthor("Dan Brown");
		firstBook.setTitle("Da Vinci Code");
		
		Book secondBook = new Book();
		firstBook.setId("a2");
		firstBook.setAuthor("J. K. Rowling");
		firstBook.setTitle("Harry Potter and the Philosopher's Stone");
		
		books.add(firstBook);
		books.add(secondBook);
	}
	
	@AfterEach
	public void clear() {
		books.clear();
	}
	
	@Test
	void getAll_ShouldGetAllBooks() {
		
		when(libraryRepository.findAll()).thenReturn(books);
		
		List<Book> bookService = libraryService.getAll();
		
		assertEquals(bookService, books);
	}
	
	@Test
	void save_ShouldSaveBook() {
		
		Book savedBook = new Book();
		savedBook.setId("1b");
		savedBook.setAuthor("Anne Frank");
		savedBook.setTitle("The Diary Of a Young Girl");
		
		when(libraryRepository.save(new Book())).thenReturn(savedBook);
		
		Book saveBook = libraryService.save(new Book());
		
		assertEquals(saveBook, savedBook);
		
	}
	
	@Test
	void updateBook_ShouldUpdateBook() {
		
		Optional<Book> savedBook = Optional.of(new Book());
		savedBook.get().setId("1b");
		savedBook.get().setAuthor("Anne Frank");
		savedBook.get().setTitle("The Diary Of a Young Girl"); 
		
		Book updateBook = new Book();
		updateBook.setAuthor("Frank, Anne");
		
		Book updatedBook = new Book();
		updatedBook.setId("1b");
		updatedBook.setAuthor("Frank, Anne");
		updatedBook.setTitle("The Diary Of a Young Girl");

		when(libraryRepository.findById("1b")).thenReturn(savedBook);
		when(libraryRepository.save(updatedBook)).thenReturn(updatedBook);
		
		Book updatedBookService = libraryService.updateBook("1b", updatedBook);
		
		assertEquals(updatedBookService, updatedBook);
		
	}
	
	@Test
	void deleteBook_ShouldDeleteBook() {
		
		Optional<Book> returnedBook = Optional.of(new Book());
		returnedBook.get().setId("1b");
		returnedBook.get().setAuthor("Frank, Anne");
		returnedBook.get().setTitle("The Diary Of a Young Girl");
		
		when(libraryRepository.findById("1b")).thenReturn(returnedBook);
		ResponseEntity<?> response = libraryService.deleteBook("1b");
		
		assertEquals(response, ResponseEntity.ok().build());
	}
	
	@Test
	void getBookById_ShouldReturnBook() {
		
		Optional<Book> returnedBook = Optional.of(new Book());
		returnedBook.get().setId("1b");
		returnedBook.get().setAuthor("Anne Frank");
		returnedBook.get().setTitle("The Diary Of a Young Girl"); 

		
		when(libraryRepository.findById("1b")).thenReturn(returnedBook);
		
		Book getBook = libraryService.getBookById("1b");
		
		assertEquals(returnedBook.get(), getBook);
	}

}
