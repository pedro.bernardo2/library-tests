package com.avenuecode.library.controller;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.avenuecode.library.entity.Book;
import com.avenuecode.library.service.LibraryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(LibraryController.class)
public class LibraryControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private LibraryService libraryService;
	
	List<Book> books = new ArrayList<>();
	
	@BeforeEach
	public void setup() {
		
		Book firstBook = new Book();
		firstBook.setId("a1");
		firstBook.setAuthor("Dan Brown");
		firstBook.setTitle("Da Vinci Code");
		
		Book secondBook = new Book();
		firstBook.setId("a2");
		firstBook.setAuthor("J. K. Rowling");
		firstBook.setTitle("Harry Potter and the Philosopher's Stone");
		
		books.add(firstBook);
		books.add(secondBook);
		
	}
	
	@AfterEach
	public void clear() {
		books.clear();
	}
	
	@Test
	public void getAll_ShouldReturnListBooks() throws JsonProcessingException, Exception {
		
		when(libraryService.getAll()).thenReturn(books);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/library"))
					.andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(books)));
	}
	
	@Test
	public void saveBook_ShouldSaveBook() throws JsonProcessingException, Exception {
		
		Book saveBook = new Book();
		saveBook.setAuthor("Anne Frank");
		saveBook.setTitle("The Diary Of a Young Girl");
		
		Book savedBook = new Book();
		savedBook.setId("1b");
		savedBook.setAuthor("Anne Frank");
		savedBook.setTitle("The Diary Of a Young Girl");

        when(libraryService.save(saveBook)).thenReturn(savedBook);

        mockMvc.perform(MockMvcRequestBuilders.post("/library")
                .contentType(MediaType.APPLICATION_JSON)
                	.content(objectMapper.writeValueAsString(saveBook)))
            			.andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(savedBook)));

	}
	
	@Test
	public void updateBook_ShouldUpdateBook() throws JsonProcessingException, Exception {
		
		Book updateBook = new Book();
		updateBook.setAuthor("Frank, Anne");
		
		Book updatedBook = new Book();
		updatedBook.setId("1b");
		updatedBook.setAuthor("Frank, Anne");
		updatedBook.setTitle("The Diary Of a Young Girl");

        when(libraryService.updateBook("1b", updateBook)).thenReturn(updatedBook);

        mockMvc.perform(MockMvcRequestBuilders.put("/library/{id}", "1b")
                .contentType(MediaType.APPLICATION_JSON)
	            	.content(objectMapper.writeValueAsString(updateBook)))
	        			.andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(updatedBook)));

	}
	
    @Test
    public void deleteBook_ShouldDeleteBook() throws JsonProcessingException, Exception {
        when(libraryService.deleteBook("1b")).thenReturn(ResponseEntity.ok().build());
        
        mockMvc.perform(MockMvcRequestBuilders.delete("/library/{id}", "1b")
                .contentType(MediaType.APPLICATION_JSON))
                	.andExpect(MockMvcResultMatchers.status().isOk());
 
    }
    
    @Test
    public void getBookById_ShouldReturnBook() throws JsonProcessingException, Exception {
    	
		Book book = new Book();
		book.setId("1b");
		book.setAuthor("Anne Frank");
		book.setTitle("The Diary Of a Young Girl");
    	
    	when(libraryService.getBookById("1b")).thenReturn(book);
    	
    	mockMvc.perform(MockMvcRequestBuilders.get("/library/{id}", "1b"))
    			.andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(book)));
    }
 
	
}
