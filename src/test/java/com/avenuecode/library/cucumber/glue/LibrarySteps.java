package com.avenuecode.library.cucumber.glue;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import com.avenuecode.library.entity.Book;
import com.avenuecode.library.repository.LibraryRepository;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LibrarySteps {
	
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@Autowired
	private LibraryRepository libraryRepository;
	
	Book bookFound;
	
	Book newBook;

	private List<Book> booksGiven;
	
	private List<Book> booksReturned;

	@Before
	public void setup() {
		booksGiven = new ArrayList<>();
		booksReturned = new ArrayList<>();
		bookFound = new Book();
	}
	
	@Given("the following books")
	public void givenTheFollowingBooks(List<Book> followingBooks) {
		followingBooks.forEach(book -> booksGiven.add(book));
		assertNotNull(followingBooks);
	}

	@When("the user requests all the books")
	public void whenTheUserRequestsAllTheBooks() {

		booksReturned = testRestTemplate.exchange("/library", HttpMethod.GET, new HttpEntity(new HttpHeaders()),
				new ParameterizedTypeReference<List<Book>>() {
									}).getBody();
		
		assertNotNull(booksReturned);

	}

	@Then("all the books are returned")
	public void thenAllTheBooksAreReturned() {

		assertEquals(booksReturned, booksGiven);

	}

	@Given("the user want to update the Book title with id {string}")
	public void givenTheUserWantsToUpdateTheBook(String bookId) {

		bookFound = testRestTemplate.exchange("/library/" + bookId, 
				HttpMethod.GET, new HttpEntity(new HttpHeaders()),Book.class).getBody();
		
		assertNotNull(bookFound);
		
	}
	
	@When("the system gets the new title {string}")
	public void whenTheSystemGetsTheNewTitle(String title) {
		
		Optional<Book> book = libraryRepository.findById(bookFound.getId());
		book.get().setTitle(title);
		assertNotEquals(book, bookFound);
		bookFound = book.get();
		
	}
	
	@Then("the user gets the updated Book")
	public void thenUsergetsTheUpdatedBook() {
		
		assertNotNull(bookFound);
		
	}
	
	@When("the user want to save the Book")
	public void whenTheUserWantSaveBook(Book book) {
		
		newBook = testRestTemplate.postForObject("/library",book, Book.class);
		
		assertEquals(newBook, book);
		
	}
	
	@Then("the user gets the Book saved")
	public void thenTheUserGetsTheBook() {
		
		assertNotNull(newBook);
		
	}

}
